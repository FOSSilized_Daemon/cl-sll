### DESCRIPTION
*cl-sll* is a logging library written in ANSI-compliant Common Lisp. *cl-sll* is written to make logging
messages as easy and standardized as possible by offering simple functions to log complex messages with vital
information such as an objects' memory address. All logging functions within *cl-sll* follow the exact same
message format shown below.
```
YEAR/MONTH/DAY HOUR:MINUTE:SECOND [SEVERITY] FILE-PATH MESSAGE {MEMORY ADDRESS OF OBJECT}
```
*cl-sll* is licensed under the [AGPLv3](./LICENSE).

### DEPENDENCIES
*cl-sll* utilizes *[ASDF](https://asdf.common-lisp.dev/)* to load its source tree and make itself available for use
within a Common Lisp environment. Due to this requirement, if one wants to use the loading mechanism provided within
*cl-sll* then one must [install *ASDF*](https://asdf.common-lisp.dev/#downloads).

### INSTALLATION
*cl-sll* can currently only be installed from source, as it has yet to be packaged for quicklisp or any
other package managers. To install *cl-sll* one simply needs to pull the source code into a directory that is
within their [*ASDF* registry](https://www.sbcl.org/asdf/Using-asdf-to-load-systems.html) and then use `make`
to install it.
```
$ git clone https://gitlab.com/FOSSilized_Daemon/cl-sll.git /path/to/asdf/registry/directory</code>
```
Then, one can use `make` to install *cl-sll*.
```
$ cd cl-sll
$ make install

```
Alternatively, one can view the source tree for *cl-sll* here.

### DOCUMENTATION
In an effort to ensure that *cl-sll* is as maintained as possible an extensive effort has been made to provide detailed
documentation for the entire library. Full documentation for *cl-sll* can be found here.

### CONTRIBUTING
As a part of the Core Project *cl-sll* is open to contributions, but only when said contributions are done within
our guidelines. In order to contribute to *cl-sll* one must **always follow the Core Project's Code of Conduct**.
Following this code of conduct is non-negotiable. All code written for *cl-sll* must follow the Core Project's
style guide for Common Lisp and be published under the [AGPLv3](./LICENSE). Finally, if one has any questions regarding
contributing to *cl-sll* FOSSilized_Daemon can be reached at their e-mail *fossilizeddaemon at
protonmail dot come*.
