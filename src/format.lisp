(in-package :sll)

;;;; The format.lisp file implements variables, functions, and other
;;;; functionality for log message formatting on operating systems
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and reprogrammable
;;;; from any ANSI compliant Common Lisp implementation.
(defun get-memory-address-of (target-object)
  "Returns the memory address of TARGET-OBJECT
   if the current Common Lisp implementation
   supports it."
  (or #+clisp (system:address-of target-object)
      #+sbcl  (sb-kernel:get-lisp-obj-address target-object)))

(defun format-message (target-message &key (level :debug)
                                           (object nil))
  "Returns a formatted string of TARGET-MESSAGE
   with the proper LEVEL. Optionally, adds the
   memory address of OBJECT."
  (let ((formed-message nil))
    (multiple-value-bind
      (second minute hour month day year)
      (decode-universal-time (get-universal-time))

      (setf formed-message (format nil "~d/~d/~d ~d:~d:~d [~A]~@[ ~A~] ~A~@[ {~A}~]"
                                       year day month hour minute second (symbol-name level)
                                       *load-pathname* target-message (when object
                                                                        (get-memory-address-of object)))))
    formed-message))
