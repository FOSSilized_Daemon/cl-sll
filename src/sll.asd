(defsystem "sll"
  :name "sll"
  :version "1.0.0"
  :maintainer "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :author "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :license "AGPLv3+ (see LICENSE for details)."
  :description "A logging library for Common Lisp."
  :long-description "A logging library for Common Lisp."
  :components ((:file "package")
               (:file "util")
               (:file "severity")
               (:file "format")
               (:file "log")))
