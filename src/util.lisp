(in-package :sll)

;;;; The util.lisp file implements variables, functions, and other
;;;; functionality from cl-os that is needed for cl-sll. Due to
;;;; the lack of effort from ASDF to address the need to interdependence
;;;; the needed code from cl-os cannot be called from cl-ssl as
;;;; it is used within cl-os. The code found within this file
;;;; is not exposed beyond its need for log.lisp. The entirety
;;;; of the below codebase is written to be easily understood
;;;; and reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defvar *stdin*
  (or #+clasp   (make-synonym-stream 'ext::+process-standard-input+)
      #+clozure (make-synonym-stream 'ccl:*stdin*)
      #+cmucl   (make-synonym-stream 'system:*stdin*)
      #+ecl     (make-synonym-stream 'ext::+process-standard-input+)
      #+sbcl    (make-synonym-stream 'sb-sys:*stdin*)
      #+scl     (make-synonym-stream 'system:*stdin*)
      #-(or clasp clozure cmucl ecl sbcl scl) (unsupported-implementation "cl-sll" "*STDIN*"))
  "A synonymous stream of standard-input.")

(defvar *stdout*
  (or #+clasp   (make-synonym-stream 'ext::+process-standard-output+)
      #+clozure (make-synonym-stream 'ccl:*stdout*)
      #+cmucl   (make-synonym-stream 'system:*stdout*)
      #+ecl     (make-synonym-stream 'ext::+process-standard-output+)
      #+sbcl    (make-synonym-stream 'sb-sys:*stdout*)
      #+scl     (make-synonym-stream 'system:*stdout*)
      #-(or clasp clozure cmucl ecl sbcl scl) (unsupported-implementation "cl-sll" "*STDOUT*"))
  "A synonymous stream of standard-output.")

(defvar *stderr*
  (or #+clasp   (make-synonym-stream 'ext::+process-error-output+)
      #+clozure (make-synonym-stream 'ccl:*stderr*)
      #+cmucl   (make-synonym-stream 'system:*stderr*)
      #+ecl     (make-synonym-stream 'ext::+process-error-output+)
      #+sbcl    (make-synonym-stream 'sb-sys:*stderr*)
      #+scl     (make-synonym-stream 'system:*stderr*)
      #-(or clasp clozure cmucl ecl sbcl scl) (unsupported-implementation "cl-sll" "*STDERR*"))
  "A synonymous stream of standard-error.")

(defun finish-outputs (&rest output-streams)
  "Ensures any buffered output has reached its destination."
  (setf output-streams (append output-streams
                              (list *stdout* *stderr* *standard-output* *error-output* *trace-output*
                                    *debug-io* *terminal-io* *query-io*)))

  (loop for target-stream in output-streams
    do (ignore-errors (finish-output target-stream))))

(defun quit (&optional (status-code 0)
                       (finish-buffered-output t))
  "Quits the current Common Lisp process."
  (when finish-buffered-output
    (finish-outputs))

  (or #+allegro      (excl:exit status-code :quit t)
      #+abcl         (ext:quit :status status-code)
      #+clasp        (si:quit status-code)
      #+clisp        (ext:quit status-code)
      #+clozure      (ccl:quit status-code)
      #+cmucl        (unix:unix-exit status-code)
      #+cormanlisp   (win32:exitprocess status-code)
      #+ecl          (ext:quit status-code)
      #+gcl          (system:quit status-code)
      #+lispworks    (libsworks:quit :status status-code :confirm nil :return nil :ignore-errors-p t)
      #+mcl          (progn status-code (ccl:quit))
      #+mkcl         (mk-ext:quit :exit-code status-code)
      #+openmcl      (progn status-code (ccl:quit))
      #+poplog       (progn status-code (poplog:bye))
      #+sbcl         (sb-ext:quit :unix-status status-code :recklessly-p (not finish-buffered-output))
      #+scl          (unix:unix-exit status-code)
      #+ufasoft-lisp (progn status-code (ext:quit))
      #+wcl          (progn status-code (lisp:quit))
      #+xcl          (ext:quit :status status-code)
      (unsupported-implementation "cl-sll" "(QUIT)")))
