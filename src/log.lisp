(in-package :sll)

;;;; The log.lisp file implements variables, functions, and other
;;;; functionality for logging information on operating systems
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and reprogrammable
;;;; from any ANSI compliant Common Lisp implementation.

(defun log-to-file (target-file target-message &key (append nil))
  "Writes TARGET-MESSAGE to TARGET-FILE by either
   appending to it or overwriting it (default)."
  (let ((target-pathname (pathname target-file)))
    (ensure-directories-exist target-file)

    (with-open-file (filespec target-pathname :direction :output :if-exists (if (not append)
                                                                              :supersede
                                                                              :append)
                                              :if-does-not-exist :create)
        (format filespec "~A~%" target-message))))

(defun log-message (target-message &key (stream *debug-io*)
                                        (file   nil)
                                        (level  :debug)
                                        (object nil)
                                        (append nil))
  "Sends TARGET-MESSAGE to either a specified STREAM
   or FILE with a severity level of LEVEL."
  (when (severity-level-p level)
    (if file
      (log-to-file file (format-message target-message) :append append)
      (progn
        (format (case level
                  (:debug (or stream *debug-io*))
                  (:info  (or stream *stderr*))
                  (:warn  (or stream *stderr*))
                  (:error (or stream *stderr*)))
                (format-message target-message :level level :object object))))))

(defmacro generate-log-function (function-name target-stream target-level)
  "Generates a standard logging function
   with all desired arguments and functionality."
  `(defun ,function-name (target-message &key (stream ,target-stream)
                                              (file   nil)
                                              (object nil)
                                              (append nil))
     (if file
       (log-message target-message :file file :level ,target-level :object object :append append)
       (log-message target-message :stream stream :level ,target-level :object object :append append))))

(generate-log-function dbg *debug-io* :debug)
(generate-log-function inf *stderr*   :info)
(generate-log-function wrn *stderr*   :warn)
(generate-log-function err *stderr*   :error)

(defun die (target-message &key (stream *error-output*)
                                (file   nil)
                                (object nil)
                                (append nil)
                                (status-code 1))
  "Sends TARGET-MESSAGE to either a specified STREAM
   or FILE with a severity level of ERROR and
   then exits the current Common Lisp process."
   (if file
     (log-message target-message :file file :level :error :object object :append append)
     (log-message target-message :stream stream :level :error :object object))
  (quit status-code))

(defun unsupported-implementation (target-feature target-library &key (file nil)
                                                                      (append nil))
  "Sends an ERROR message stating the current
   Common Lisp implementation is not supported
   for TARGET-FEATURE by TARGET-LIBRARY to *STDERR*
   before exiting the current Common Lisp process."
   (if file
     (die (format nil "The current Common Lisp implementation is not supported by ~A for ~A. Please contact the developers or one's department head for support." target-library target-feature) :file file :append append)
     (die (format nil "The current Common Lisp implementation is not supported by ~A for ~A. Please contact the developers or one's department head for support." target-library target-feature))))
