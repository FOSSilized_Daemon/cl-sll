(defpackage :sll
  (:use :common-lisp :asdf)
  (:export
    ; severity.lisp
    #:*severity-limit*

    ; log.lisp
    #:dbg
    #:inf
    #:wrn
    #:err
    #:die
    #:unsupported-implementation))
