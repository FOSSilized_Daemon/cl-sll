## The current version of cl-sll.
VERSION=1.0.0

## Common Lisp non-interactive command-line
## call.
LISP_CMD=sbcl --non-interactive --eval
