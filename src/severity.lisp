(in-package :sll)

;;;; The severity.lisp file implements variables, functions, and other
;;;; functionality for log severity level information on operating systems
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and reprogrammable
;;;; from any ANSI compliant Common Lisp implementation.

(defvar *severity-limit* 4
  "The current logging severity limit.")

(deftype severity-levels ()
  "Contains the supported logging severity levels."
  '(member :debug :info :warn :error))

(defun get-level-severity (target-level)
  "Returns the severity level of TARGET-LEVEL."
  (declare (type severity-levels target-level))

  (ecase target-level
    ((:debug) 3)
    ((:info)  2)
    ((:warn)  1)
    ((:error) 0)))

(defun severity-level-p (target-level)
  "Determines if TARGET-LEVEL is less than or
   equal to *SEVERITY-LIMIT*."
   (and (>= *severity-limit* (get-level-severity target-level))
        (not (> *severity-limit* 4))))
